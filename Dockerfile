ARG AUTO_BUILD_IMAGE="registry.gitlab.com/gitlab-org/cluster-integration/auto-build-image"
ARG AUTO_BUILD_IMAGE_VERSION="v1.51.0"
FROM $AUTO_BUILD_IMAGE:$AUTO_BUILD_IMAGE_VERSION

# remove all references to 'latest' in the build script
RUN sed -i.bck '/image_latest/d' /build/build.sh
