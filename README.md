# IGWN Docker CI/CD templates

This project contains the CI/CD templates for building IGWN
Docker images.

## Basic usage

To use the template in your project, configure your `.gitlab-ci.yml`
file as follows:

```yaml
include:
  - project: docker/cicd
    file: 'IGWN-Docker.gitlab-ci.yml'
    inputs:
      project-path: <namespace>/<project>
```

This will create a pipeline with the following jobs

| Job                  | Stage    | Purpose |
| -------------------- | -------- | ------- |
| `build`              | `build`  | Build a docker image for the project [^1] |
| `container_scanning` | `scan`   | Scan the image for vulnerabilities |
| `push:containers`    | `deploy` | Push a tagged version of the image to the container registry [^2][^3] |

[^1]: the built image is pushed to the container registry with the name `$CI_PROJECT_PATH:$CI_COMMIT_SHA`,
[^2]: the new tag has the value `$CI_COMMIT_REF_SLUG`, which tracks either the git tag (for tag pipelines) or the commit branch name.
[^3]: by default this job only runs for tag pipelines and branch pipelines on the default branch, see _Push all branches_ below for more details.

## Customisations

### Custom image name

To customise the image name used when pushing to the gitlab container registry,
set `CI_APPLICATION_REPOSITORY` in the global `variables` section, e.g::

```yaml
variables:
  CI_APPLICATION_REPOSITORY: $CI_REGISTRY_IMAGE/api
```

### Custom image tag

To customise the tag used when pushing to any container registry,
set `CI_APPLICATION_TAG` in the global `variables` section, e.g:

```yaml
variables:
  CI_APPLICATION_TAG: "science"
```

### Custom Dockerfile path

To customise the path used to discover the docker configuration file,
set `DOCKERFILE_PATH` in the global `variables` section:

```yaml
variables:
  DOCKERFILE_PATH: "docker/Dockerfile"
```

### Don't push the latest tag

### Push all branches, not just the default branch

If you want to push images for all branches, not just the default branch,
set `push-all-branches: true` in your `inputs` when including the template:

```yaml
include:
  - project: docker/cicd
    file: 'IGWN-Docker.gitlab-ci.yml'
    inputs:
      project-path: docker/base
      push-all-branches: true
```

This will result in the `deploy`-stage jobs running for any branch pipeline on the
upstream project (identified by the `project-path` input).

## Pushing to external container registries

### Docker Hub

To configure your pipeline to push the tagged image to Docker Hub, include
`docker-io-repository` in your `inputs`, e.g:

```yaml
include:
  - project: docker/cicd
    file: 'IGWN-Docker.gitlab-ci.yml'
    inputs:
      project-path: docker/base
      docker-io-repository: igwn/base
```

and add the following variables as
[project variables](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project):

| Name               | Purpose |
| ------------------ | ------- |
| `DOCKER_HUB_USER`  | The username to user when authenticating to <https://hub.docker.com> with `docker login` |
| `DOCKER_HUB_TOKEN` | The [access token](https://docs.docker.com/docker-hub/access-tokens/) to use when authenticating |

This will add a new `push:docker_io` job to the `deploy` stage of the pipeline.

### Red Hat Quay.io

To configure your pipeline to push the tagged image to Red Hat Quay.io, include
`quay-io-repository` in your `inputs`, e.g::

```yaml
include:
  - project: docker/cicd
    file: 'IGWN-Docker.gitlab-ci.yml'
    inputs:
      project-path: docker/base
      quay-io-repository: igwn/base
```

and add the following variables as
[project variables](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project):

| Name         | Purpose |
| ------------ | ------- |
| `QUAY_USER`  | The username to user when authenticating to <https://quay.io> with `docker login` |
| `QUAY_TOKEN` | The robot account [password](https://docs.quay.io/glossary/robot-accounts.html) to use when authenticating |

This will add a new `push:quay_io` job to the `deploy` stage of the pipeline.

---
